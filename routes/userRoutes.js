const express = require('express');

const router = express.Router();

const userControllers = require('../controllers/userControllers');

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUsersController);

router.get('/getSingleUser/:id', userControllers.getSingleUserController);

router.put('/updateUserStatus/:id', userControllers.updateUserStatusController);

module.exports = router;






